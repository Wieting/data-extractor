package tokenize;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class ExtractTestingData {

	//get tokens and scores for following datasets:
	// STS tasks
	// AnnoPPDB
	// JHU PPDB
	// Twitter
	// SICK
	public static void main(String[] args) {
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		props.put("tokenize.options", "americanize=true,strictTreebank3=true");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		ArrayList<Example> examples = new ArrayList<Example>();
		HashSet<String> vocab = ExtractPPDB.getVocab("glove.840B.vocab");
		ExtractPPDB.vocab = vocab;
		String outdir = "data_tokenized/";
		
		//load list of datasets
		FileReader fileReader = null;
		String line;

		HashSet<String> lis = new HashSet<String>();
		try {
			fileReader = new FileReader("datasets.txt");

			BufferedReader bufferedReader = new BufferedReader(fileReader);
			while ((line = bufferedReader.readLine()) != null) {
				lis.add(line);
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String prefix = "/Users/Wieting/Desktop/theano2/";
		
		for(String s: lis) {
			if(s.length() > 0 && !s.startsWith("#")) {
				//System.out.println(s);
				String[] arr = s.split("\\s+");
				ArrayList<String> data = getSTSData(prefix+arr[0],prefix+arr[1],pipeline);
				String fout = outdir+arr[2];
				writeFile(fout,data);
			}
		}
		
		ArrayList<String> data = getSICKdata(prefix+"dataset/SICK_test_annotated.txt", pipeline);
		writeFile(outdir+"sicktest",data);
		data = getBigrams(prefix+"dataset/bigrams_jn.txt",pipeline);
		writeFile(outdir+"bigram-jn",data);
		data = getBigrams(prefix+"dataset/bigrams_nn.txt",pipeline);
		writeFile(outdir+"bigram-nn",data);
		data = getBigrams(prefix+"dataset/bigrams_vn.txt",pipeline);
		writeFile(outdir+"bigram-vn",data);
		data = getAnnoPPDB(prefix+"dataset/ppdb_dev.txt",pipeline);
		writeFile(outdir+"anno-dev",data);
		data = getAnnoPPDB(prefix+"dataset/ppdb_test.txt",pipeline);
		writeFile(outdir+"anno-test",data);
		data = getJHUPPDB(prefix+"dataset/ppdb-sample.tsv",pipeline);
		writeFile(outdir+"JHUppdb",data);
		data = getTwitter(prefix+"dataset/test.data", prefix+"dataset/test.label",pipeline);
		writeFile(outdir+"twitter",data);
		
		ArrayList<String> all_words = new ArrayList<String>(ExtractPPDB.all_words);
		writeFile("all_vocab.txt",all_words);
	}

	private static void writeFile(String fout, ArrayList<String> data) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(fout, "UTF-8");
			for(String d: data)
				writer.println(d);
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static ArrayList<String> getAnnoPPDB(String fname, StanfordCoreNLP pipeline) {
		ArrayList<String> data = new ArrayList<String>();
		
		try {
			ArrayList<String> lines = LineIO.read(fname);
			
			for(int i=0; i < lines.size(); i++) {
				String[] arr = lines.get(i).split("\\|\\|\\|");
				String t1 = ExtractPPDB.getTokenString(arr[0], pipeline);
				String t2 = ExtractPPDB.getTokenString(arr[1], pipeline);
				String score = arr[2];
				String out = t1+"\t"+t2+"\t"+score;
				//System.out.println(out);
				data.add(out);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;
	}
	
	private static ArrayList<String> getJHUPPDB(String fname, StanfordCoreNLP pipeline) {
			ArrayList<String> data = new ArrayList<String>();
		
		try {
			ArrayList<String> lines = LineIO.read(fname);
			
			for(int i=0; i < lines.size(); i++) {
				String[] arr = lines.get(i).split("\\t");
				String t1 = ExtractPPDB.getTokenString(arr[2], pipeline);
				String t2 = ExtractPPDB.getTokenString(arr[3], pipeline);
				String score = arr[0];
				String out = t1+"\t"+t2+"\t"+score;
				//System.out.println(out);
				data.add(out);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;
	}

	private static ArrayList<String> getTwitter(String fname, String g, StanfordCoreNLP pipeline) {
		ArrayList<String> data = new ArrayList<String>();
		
		try {
			ArrayList<String> lines = LineIO.read(fname);
			ArrayList<String> gold = LineIO.read(g);
			
			for(int i=0; i < lines.size(); i++) {
				String[] arr = lines.get(i).split("\\t");
				String t1 = ExtractPPDB.getTokenString(arr[2], pipeline);
				String t2 = ExtractPPDB.getTokenString(arr[3], pipeline);
				String score = gold.get(i).split("\\s+")[1];
				String out = t1+"\t"+t2+"\t"+score;
				//System.out.println(out);
				data.add(out);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;
	}

	private static ArrayList<String> getBigrams(String fname, StanfordCoreNLP pipeline) {
		ArrayList<String> data = new ArrayList<String>();
		
		try {
			ArrayList<String> lines = LineIO.read(fname);
			
			for(int i=1; i < lines.size(); i++) {
				String[] arr = lines.get(i).split("\\t");
				String t1 = ExtractPPDB.getTokenString(arr[0], pipeline);
				String t2 = ExtractPPDB.getTokenString(arr[1], pipeline);
				String score = arr[2];
				String out = t1+"\t"+t2+"\t"+score;
				//System.out.println(out);
				data.add(out);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;
	}

	private static ArrayList<String> getSICKdata(String fname, StanfordCoreNLP pipeline) {
		ArrayList<String> data = new ArrayList<String>();
		
		try {
			ArrayList<String> lines = LineIO.read(fname);
			
			for(int i=1; i < lines.size(); i++) {
				String[] arr = lines.get(i).split("\\t");
				//System.out.println(arr[1]);
				String t1 = ExtractPPDB.getTokenString(arr[1], pipeline);
				String t2 = ExtractPPDB.getTokenString(arr[2], pipeline);
				String score = arr[3];
				String out = t1+"\t"+t2+"\t"+score;
				//System.out.println(out);
				data.add(out);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;
	}

	public static ArrayList<String> getSTSData(String text, String gold, StanfordCoreNLP pipeline) {
		
		ArrayList<String> data = new ArrayList<String>();
		
		try {
			ArrayList<String> lines = LineIO.read(text);
			ArrayList<String> golds = LineIO.read(gold);
			
			for(int i=0; i < lines.size(); i++) {
				String[] arr = lines.get(i).split("\t");
				//System.out.println(lines.get(i));
				String t1 = ExtractPPDB.getTokenString(arr[0], pipeline);
				String t2 = ExtractPPDB.getTokenString(arr[1], pipeline);
				String score = golds.get(i);
				if(score.trim().length() > 0) {
					String out = t1+"\t"+t2+"\t"+score;
					data.add(out);
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return data;
	}
}
