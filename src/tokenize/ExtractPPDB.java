package tokenize;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;


public class ExtractPPDB {
	
	public static HashSet<String> all_words = new HashSet<String>();
	public static HashSet<String> vocab = new HashSet<String>();
	
	public static void main(String[] args) {

		String fname = "/Users/Wieting/Downloads/ppdb-1.0-xl-phrasal";

		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		props.put("tokenize.options", "americanize=true,strictTreebank3=true");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		FileReader fileReader = null;
		String line;

		ArrayList<Example> examples = new ArrayList<Example>();
		HashSet<String> vocab = getVocab("glove.840B.vocab");

		HashSet<String> in = new HashSet<String>();
		HashSet<String> out = new HashSet<String>();
		HashSet<String> ppdb = new HashSet<String>();
		
		System.out.println(vocab.size());
		HashSet<String> phrase_pairs = new HashSet<String>();
		
		try {
			fileReader = new FileReader(fname);

			BufferedReader bufferedReader = new BufferedReader(fileReader);
			int ct = 0;
			while ((line = bufferedReader.readLine()) != null) {
				ct += 1;
				String[] arr = line.split("\\|\\|\\|");
				String p1 = arr[1];
				String p2 = arr[2];
				//String[] t1 = p1.split("\\s+");
				//String[] t2 = p2.split("\\s+");
				ArrayList<String> tok1 = getTokens(p1,pipeline);
				ArrayList<String> tok2 = getTokens(p2,pipeline);

				String st1 = "";
				for(String s: tok1) {
					s = s.toLowerCase();
					if(s.equals("-lsb-"))
						s = "[";
					if(s.equals("-rsb-"))
						s = "]";
					if(s.equals("-lrb-"))
						s = "(";
					if(s.equals("-rrb-"))
						s = ")";
					if(!vocab.contains(s))
						out.add(s);
					else
						in.add(s);
					
					st1 += s+" ";
				}

				String st2 = "";
				for(String s: tok2) {
					s = s.toLowerCase();
					if(s.equals("-lsb-"))
						s = "[";
					if(s.equals("-rsb-"))
						s = "]";
					if(s.equals("-lrb-"))
						s = "(";
					if(s.equals("-rrb-"))
						s = ")";
					if(!vocab.contains(s))
						out.add(s);
					else
						in.add(s);
					st2 += s+" ";
				}
				
				//System.out.println(in.size());
				//System.out.println(out.size());

				if(ct % 1000 == 0)
					System.out.println(ct);
				
				String ppdbout = null;
				if(st1.compareTo(st2) < 0)
					ppdbout = st1+"\t"+st2;
				else
					ppdbout = st2+"\t"+st1;
				
				ppdb.add(ppdbout);
				//System.out.println(st1+"\t"+st2);
			}
		}catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		System.out.println(in.size());
		System.out.println(out.size());
		
		PrintWriter writer;
		try {
			writer = new PrintWriter("ppdb-out-vocab.txt", "UTF-8");
			for(String s: out)
				writer.println(s);
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			writer = new PrintWriter("ppdb-in-vocab.txt", "UTF-8");
			for(String s: in)
				writer.println(s);
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			writer = new PrintWriter("ppdb-in-vocab.txt", "UTF-8");
			for(String s: in)
				writer.println(s);
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		try {
			writer = new PrintWriter("ppdb-XL-data.txt", "UTF-8");
			for(String s: ppdb)
				writer.println(s);
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	public static HashSet<String> getVocab(String fname) {

		FileReader fileReader = null;
		String line;

		HashSet<String> lis = new HashSet<String>();
		try {
			fileReader = new FileReader(fname);

			BufferedReader bufferedReader = new BufferedReader(fileReader);
			while ((line = bufferedReader.readLine()) != null) {
				String s = line.toLowerCase().trim();
				lis.add(s);
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//return new ArrayList<String>(lis);
		return lis;
	}
	
	public static String getTokenString(String s, StanfordCoreNLP pipeline) {
		ArrayList<String> tok1 = getTokens(s,pipeline);

		String st = "";
		for(String t: tok1) {
			s = t.toLowerCase();
			if(s.equals("-lsb-"))
				s = "[";
			if(s.equals("-rsb-"))
				s = "]";
			if(s.equals("-lrb-"))
				s = "(";
			if(s.equals("-rrb-"))
				s = ")";
			
			all_words.add(s);
			if(!vocab.contains(s)) {
				//System.out.println(s);
			}
			st += s+" ";
		}
		
		return st;
	}

	public static ArrayList<String> getTokens(String s, StanfordCoreNLP pipeline) {
		Annotation document = new Annotation(s);
		pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		ArrayList<String> tokens = new ArrayList<String>();

		for(CoreMap sentence: sentences) {

			List<CoreLabel> lis = sentence.get(TokensAnnotation.class);
			int t=0;
			for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
				String word = token.get(TextAnnotation.class);
				tokens.add(word);
			}
		}

		return tokens;
	}
}
