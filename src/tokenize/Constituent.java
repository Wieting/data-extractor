package tokenize;

import java.util.ArrayList;

public class Constituent {

	ArrayList<String> tokens;
	
	public Constituent() {
		
	}
	
	public String getString() {
		String s = "";
		
		for(String tok: tokens) {
			s += tok + " ";
		}
		
		return s;
	}
	
}
